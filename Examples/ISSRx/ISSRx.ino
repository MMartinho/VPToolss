#include <ArduinoJson.h>
#include <Arduino.h>
#include <SPI.h>
#include <EEPROM.h>
#include <Wire.h>
#include "DavisRFM69.h"
#include "PacketFifo.h"

#define LED 9 // Moteinos have LEDs on D9
#define SERIAL_BAUD 115200

DavisRFM69 radio;

// id, type, active
Station stations[3] = {
    {0, STYPE_ISS, true},
    {1, STYPE_WLESS_ANEMO, true},
    {2, STYPE_TEMP_ONLY, true},
};

void setup()
{
  Serial.begin(SERIAL_BAUD);
  radio.setStations(stations, 2);
  radio.initialize(FREQ_BAND_US);
  radio.setBandwidth(RF69_DAVIS_BW_NARROW);
  radio.setRssiThreshold(-95);
}

void loop()
{
  if (radio.fifo.hasElements())
  {
    decode_packet(radio.fifo.dequeue());
  }
}

void Blink(byte PIN, int DELAY_MS)
{
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN, HIGH);
  delay(DELAY_MS);
  digitalWrite(PIN, LOW);
}

void decode_packet(RadioData *rd)
{
  int val;
  byte *packet = rd->packet;
  DynamicJsonDocument doc(1024);

  doc["raw"] = printHex(packet, 10);
  doc["station"] = packet[0] & 0x7;

  doc["packets"] = radio.packets;
  doc["lostPackets"] = radio.lostPackets;
  doc["packetRatio"] = (float)(radio.packets * 100.0 / (radio.packets + radio.lostPackets));

  doc["channel"] = rd->channel;
  doc["rssi"] = -rd->rssi;

  doc["batt"] = (char *)(packet[0] & 0x8 ? "err" : "ok");

  byte id = radio.DATA[0] & 7;
  int stIx = radio.findStation(id);

  if (packet[2] != 0)
  {
    if (stations[stIx].type == STYPE_VUE)
    {
      val = (packet[2] << 1) | (packet[4] & 2) >> 1;
      val = round(val * 360 / 512);
    }
    else
    {
      val = 9 + round((packet[2] - 1) * 342.0 / 255.0);
    }
  }
  else
  {
    val = 0;
  }
  doc["windv"] = packet[1];
  doc["winddraw"] = packet[2];
  doc["windd"] = val;

  switch (packet[0] >> 4)
  {

  case VP2P_UV:
    val = word(packet[3], packet[4]) >> 6;
    if (val < 0x3ff)
    {
      doc["uv"] = (float)(val / 50.0);
    }
    else
    {
      doc["uv"] = -1;
    }
    break;

  case VP2P_SOLAR:
    val = word(packet[3], packet[4]) >> 6;
    if (val < 0x3fe)
    {
      doc["solar"] = (float)(val * 1.757936);
    }
    else
    {
      doc["solar"] = -1;
    }
    break;

  case VP2P_RAIN:
    if (packet[3] == 0x80)
    {
      doc["rain"] = -1;
    }
    else
    {
      doc["rain"] = packet[3];
    }
    break;

  case VP2P_RAINSECS:
    val = (packet[4] & 0x30) << 4 | packet[3];
    if (val == 0x3ff)
    {
      doc["rainsecs"] = -1;
    }
    else
    {
      if ((packet[4] & 0x40) == 0)
        val >>= 4;
      doc["rainsecs"] = val;
    }
    break;

  case VP2P_TEMP:
    if (packet[3] == 0xff)
    {
      doc["temp"] = -100;
    }
    else
    {
      val = (int)packet[3] << 4 | packet[4] >> 4;
      doc["temp"] = (float)(val / 10.0);
    }
    break;

  case VP2P_HUMIDITY:
    val = ((packet[4] >> 4) << 8 | packet[3]) / 10;
    doc["rh"] = (float)val;
    break;

  case VP2P_WINDGUST:
    doc["windgust"] = packet[3];
    if (packet[3] != 0)
    {
      doc["gustref"] = packet[5] & 0xf0 >> 4;
    }
    break;

  case VP2P_SOIL_LEAF:
    doc["soilleaf"] = -1;
    break;

  case VUEP_VCAP:
    val = (packet[3] << 2) | (packet[4] & 0xc0) >> 6;
    doc["vcap"] = (float)(val / 300.0);
    break;

  case VUEP_VSOLAR:
    val = (packet[3] << 2) | (packet[4] & 0xc0) >> 6;
    doc["vsolar"] = (float)(val / 300.0);
    break;

  case VP2P_UNKNOWN1:
    val = packet[4];
    doc["unknown1"] = val;
  }

  doc["fei"] = round(rd->fei * RF69_FSTEP / 1000);
  doc["delta"] = rd->delta;

  serializeJson(doc, Serial);
  Serial.println();
}

String printHex(volatile byte *packet, byte len)
{
  String result = "";
  for (byte i = 0; i < len; i++)
  {
    if (!(packet[i] & 0xf0))
      result += '0';
    result += String(packet[i], HEX);
    if (i < len - 1)
      result += '-';
  }
  return result;
}